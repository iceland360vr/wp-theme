'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpCopy = require('gulp-copy');

gulp.task('scss', function () {
  return gulp.src('./assets/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./build/css'));
});

gulp.task('imgs', function () {
  return gulp.src('./assets/imgs/**/*')
    .pipe(gulpCopy('./build', {
      prefix: 1,
    }))
    .pipe(gulp.dest('imgs/'));
});

gulp.task('watch:scss', function () {
  gulp.watch('./assets/scss/**/*', ['scss']);
});

gulp.task('watch', ['watch:scss']);
gulp.task('build', ['imgs', 'scss']);
gulp.task('default', ['build', 'watch']);
