<?php
/**
 * Iceland360VR functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Iceland360VR
 */

if ( ! function_exists( 'iceland360vr_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function iceland360vr_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Iceland360VR, use a find and replace
		 * to change 'iceland360vr' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'iceland360vr', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'iceland360vr' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'iceland360vr_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'iceland360vr_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function iceland360vr_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'iceland360vr_content_width', 640 );
}
add_action( 'after_setup_theme', 'iceland360vr_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function iceland360vr_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'iceland360vr' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'iceland360vr' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'iceland360vr_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function iceland360vr_scripts() {
	$assets_version = 13;

	wp_enqueue_style( 'iceland360vr-style', get_stylesheet_uri() );

	wp_enqueue_style( 'iceland360vr-style-gulp', get_template_directory_uri() . '/build/css/base.css', [], $assets_version );
	wp_enqueue_style( 'iceland360vr-style-gulp-small', get_template_directory_uri() . '/build/css/small.css', [], $assets_version, "only screen and (max-width: 63rem)"); // before: 37.4375

	wp_enqueue_style( 'iceland360vr-typekit', 'https://use.typekit.net/mvj2awc.css');

	// wp_enqueue_style( 'iceland360vr-fa', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css');

	wp_enqueue_script( 'iceland360vr-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'iceland360vr-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'iceland360vr-search', get_template_directory_uri() . '/js/search.js', array(), '20151215', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'iceland360vr_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Set the size for the thumbnails
set_post_thumbnail_size(620, 620);

function custom_query_vars( $query ) {
  if ( $query->is_archive() ) {
		$post_type = $query->get('post_type');
    if ( $post_type == 'panorama' ) {
      $query->set( 'posts_per_page', $GLOBALS['is_home'] ? 8 : 20 );
      $query->set( 'orderby', 'title' );
      $query->set( 'order', 'ASC' );
    }
    if ( $post_type == 'virtualtour' ) {
      $query->set( 'posts_per_page', $GLOBALS['is_home'] ? 8 : 20 );
      $query->set( 'orderby', 'post_date' );
      $query->set( 'order', 'DESC' );
    }
		if ( $post_type == 'themedvirtualtour' ) {
      $query->set( 'posts_per_page', $GLOBALS['is_home'] ? 8 : 20 );
      $query->set( 'orderby', 'post_date' );
      $query->set( 'order', 'DESC' );
    }
  }
  return $query;
}
add_action( 'pre_get_posts', 'custom_query_vars' );

function wpsites_query( $query ) {
if ( $query->is_archive() && $query->is_main_query() && !is_admin() ) {
        $query->set( 'posts_per_page', 100000 );
    }
}
add_action( 'pre_get_posts', 'wpsites_query' );

function like_button() {
	global $wp;
	$url = home_url( $wp->request );

	$content = <<<END
	<div class="fb-like" data-href="{$url}" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
END;

	echo $content;
}
