(function() {
  document.addEventListener('click', function (ev) {
    var target = ev.target;

    if (target.nodeName.toLowerCase() === 'a' && target.href.match('#search')) {
      var bd = document.querySelector('body'),
          frm = document.querySelector('.iceland-search-form');
      if (bd.className.match('is-showSearch')) {
        bd.className = bd.className.replace('is-showSearch', '')

        setTimeout(function() {
          frm.setAttribute('hidden', 'hidden');
        }, 250);
      } else {
        frm.removeAttribute('hidden');

        setTimeout(function() {
          bd.className = bd.className.replace('is-showSearch', '') + ' is-showSearch';
          var el = document.getElementById('search-form');
          if (el) {
            el.focus();
          }
        }, 10);
      }
    }
  });
})();
