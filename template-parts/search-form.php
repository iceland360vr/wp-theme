<div class="iceland-search-form">
  <form id="iceland360vr-results" action="/#iceland360vr-results" method="GET">
    <p class="search-form-field">
      <label class="assist">Search keywords</label>
      <input type="search" name="s" id="search-form" placeholder="What are you looking for?"
      <?php if (!empty($_GET['s'])) {?>
        value="<?php echo htmlentities($_GET['s']); ?>"
      <?php
      } ?>>
      <button type="submit">Find it!</button>
    </p>
  </form>
</div>
