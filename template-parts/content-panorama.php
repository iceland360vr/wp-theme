<?php
  $filename = post_custom("panorama_filename");

  if (!empty($filename)) {
?>
  <iframe class="panorama-iframe" src="<?php echo content_url() . "/panoramas/" . $filename; ?> " width="800" height="600" allowFullScreen></iframe>
  <p class="small-screen-tip"><strong>Tip:</strong> Rotate your device to landscape for fullscreen.</p>
<?php
  }
?>
