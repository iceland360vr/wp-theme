<?php
/**
 * Template part for displaying Panoramas cards
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iceland360VR
 */

?>

<li class="Card">
  <a class="Card-link" href="<?php echo get_the_permalink(); ?>">
    <article>
      <header>
        <?php if ( has_post_thumbnail() ) { ?>
          <div class="ratio ratio-1x1"><?php the_post_thumbnail("post-thumbnail", ["class" => "Card-cover"]); ?></div>
        <?php } else { ?>
          <div class="ratio ratio-1x1"><span class="Card-emptyCover"></span></div>
        <?php } ?>
        <h2 class="Card-title"></i> <?php echo get_the_title(); ?></h2>
      </header>
      <div class="Card-content">
        <?php if (get_the_tags()) { ?>
          <ul class="Card-meta">
            <?php
            foreach(get_the_tags() as $tag) {
              echo "<li>{$tag->name}</li> ";
            }
            ?>
          </ul>
        <?php } ?>
      </div>
    </article>
  </a>
</li>
