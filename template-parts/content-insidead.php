<aside class="insidead">
  <div>
    <p class="insidead-text">In collaboration with </p>

    <p class="insidead-ads">
      <a title="Visit Grayline.is website" href="http://www.grayline.is" target="_blank"><img class="insidead-ad" src="<?php echo get_template_directory_uri() . '/build/imgs/ads/grayline_600px.jpg'; ?>" alt=""></a>
      <a title="Find travel tips and Tours" href="/traveltipsandtours/"><img class="insidead-ad" src="<?php echo get_template_directory_uri() . '/build/imgs/ads/travelade_600px.jpg'; ?>" alt=""></a>
    </p>
  </div>
</aside>
