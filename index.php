<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Iceland360VR
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="site-main-intro">
				<?php get_template_part('template-parts/content', 'insidead'); ?>

	 			<div class="intro">
					<p class="h1">
						<strong>Welcome to Iceland 360 VR!</strong>
					</p>
	        <p>Select one of hundreds of locations around Iceland in the
	          <a href="#search">search field</a>, <a href="/panorama/">panorama location list</a> or
	          <a href="/map/">location map</a>
	          or try out our <a href="/virtualtour/">location based</a> and
	          <a href="/themedvirtualtour/">themed virtual tours</a>!</p>
				</div>
			</div>

				<?php
				$GLOBALS['is_home'] = true;
				$loop = new WP_Query( array( 'post_type' => 'virtualtour', 'ignore_sticky_posts' => 1, 'paged' => 1, 'posts_per_page' => 2 ) );
				$loop->set('posts_per_page', 2);
				// echo '<pre>'; print_r( $loop ); echo '</pre>';
		    if ( $loop->have_posts() ) :
					?>
					<h2>Virtual tours</h2>
		      <ul class="CardList">
					<?php
		        while ( $loop->have_posts() ) : $loop->the_post();
							get_template_part( 'template-parts/card', 'panorama' );
							?>
		        <?php
						endwhile;

		    endif;
				?>
				</ul>
				<?php
		    wp_reset_postdata();
				?>
				<p class="see-more"><a href="/virtualtour">See more <span class="assist">Virtual Tours</span></a></p>

        <?php
        $loop = new WP_Query( array( 'post_type' => 'themedvirtualtour', 'ignore_sticky_posts' => 1, 'paged' => $paged ) );
        if ( $loop->have_posts() ) :
          ?>
          <h2>Themed Virtual tours</h2>
          <ul class="CardList">
          <?php
            while ( $loop->have_posts() ) : $loop->the_post();
              get_template_part( 'template-parts/card', 'panorama' );
              ?>
            <?php
            endwhile;
        endif;
        ?>
        </ul>
        <?php
        wp_reset_postdata();
        ?>
				<p class="see-more"><a href="/themedvirtualtour">See more <span class="assist">Themed Virtual Tours</span></a></p>

				<?php
				$loop = new WP_Query( array( 'post_type' => 'panorama', 'ignore_sticky_posts' => 1, 'paged' => $paged ) );
		    if ( $loop->have_posts() ) :
					?>
					<h2>Locations</h2>
					<ul class="CardList">
						<?php
		        while ( $loop->have_posts() ) : $loop->the_post();
							get_template_part( 'template-parts/card', get_post_type() );
							?>
		        <?php
						endwhile;
		      ?></ul>
				<?php
		    endif;
		    wp_reset_postdata();
				?>
				<p class="see-more"><a href="/panorama">See more <span class="assist">Locations</span></a></p>

		<?php
		// if ( have_posts() ) :
    //
		// 	if ( is_home() && ! is_front_page() ) :
		// 		? >
		// 		<header>
		// 			<h1 class="page-title screen-reader-text"><?php single_post_title(); ? ></h1>
		// 		</header>
		// 		< ?php
		// 	endif;
    //
		// 	/* Start the Loop */
		// 	while ( have_posts() ) :
		// 		the_post();
    //
		// 		/*
		// 		 * Include the Post-Type-specific template for the content.
		// 		 * If you want to override this in a child theme, then include a file
		// 		 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
		// 		 */
		// 		// get_template_part( 'template-parts/content', get_post_type() );
    //
		// 	endwhile;
    //
		// 	the_posts_navigation();
    //
		// else :
    //
		// 	get_template_part( 'template-parts/content', 'none' );
    //
		// endif;
		?>

		</main><!-- #main -->
		<?php // get_template_part('template-parts/content', 'sidead'); ?>
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
